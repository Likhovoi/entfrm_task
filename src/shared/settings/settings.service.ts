import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { SettingEntity } from '../../database/entities/setting.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as Currency from 'currency.js';

export class SettingsHolder {
  constructor(private payload: Settings) {}

  get(): Settings {
    return this.payload;
  }

  set(s: Settings) {
    this.payload = s;
  }
}

export class Settings {
  serviceFeeRate: number;
  minimumFee: Currency;
}

@Injectable()
export class SettingsService {
  public settings: Settings;

  constructor(
    @InjectRepository(SettingEntity)
    private readonly pureSettingsRepo: Repository<SettingEntity>,
  ) {}

  async fetchAndBuild(): Promise<Settings> {
    const all = await this.pureSettingsRepo.find();

    const serviceFeeRateOption = all.find((el) => el.key == 'serviceFeeRate');
    const serviceFeeRate = parseFloat(serviceFeeRateOption.value);
    if (isNaN(serviceFeeRate)) {
      throw new Error("'serviceFee' must be a number");
    }

    const minimumFeeOption = all.find((el) => el.key == 'minimumFee');
    const minimumFee = parseFloat(minimumFeeOption.value);
    if (isNaN(minimumFee)) {
      throw new Error("'minimumFee' must be a number");
    }

    const s = new Settings();
    s.serviceFeeRate = serviceFeeRate;
    s.minimumFee = Currency(minimumFee);

    return s;
  }
}
