import { Module } from '@nestjs/common';
import { SettingsHolder, SettingsService } from './settings/settings.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingEntity } from '../database/entities/setting.entity';

export const SETTINGS_TARGET = 'settings';

const settingsHolderProvider = {
  provide: SETTINGS_TARGET,
  useFactory: async (settingsSvc: SettingsService): Promise<SettingsHolder> => {
    const settings = await settingsSvc.fetchAndBuild();
    return new SettingsHolder(settings);
  },
  inject: [SettingsService],
};

@Module({
  imports: [TypeOrmModule.forFeature([SettingEntity])],
  providers: [SettingsService, settingsHolderProvider],
  exports: [SETTINGS_TARGET],
})
export class SharedModule {}
