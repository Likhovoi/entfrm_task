import { Module } from '@nestjs/common';
import { TicketTierModule } from './ticket-tier/ticket-tier.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppdataSourceOptions } from './database/data-source';

@Module({
  imports: [TypeOrmModule.forRoot(AppdataSourceOptions), TicketTierModule],
})
export class AppModule {}
