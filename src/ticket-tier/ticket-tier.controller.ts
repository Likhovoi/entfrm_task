import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { PriceInput, TicketTierService } from './ticket-tier.service';
import * as Joi from 'joi';
import * as Currency from 'currency.js';
import { TicketTierEntity } from '../database/entities/ticketTier.entity';

class PriceDTO {
  buyerPrice: number;
  promoterRevenue: number;
}

const creationValidator = Joi.object({
  buyerPrice: Joi.number().greater(0).precision(2),
  promoterRevenue: Joi.number().greater(0).precision(2),
}).xor('buyerPrice', 'promoterRevenue');

@Controller('ticket-tier')
export class TicketTierController {
  constructor(private readonly ticketTierSvc: TicketTierService) {}

  @Post()
  @HttpCode(201)
  async create(@Body() body: PriceDTO, @Res() response: Response) {
    const validity = creationValidator.validate(body);
    if (validity.error) {
      response.status(HttpStatus.BAD_REQUEST).json(validity.error.details);
      return;
    }

    const priceInput = new PriceInput();

    if (body.buyerPrice !== undefined) {
      priceInput.buyerPrice = Currency(body.buyerPrice);
    } else {
      priceInput.promoterRevenue = Currency(body.promoterRevenue);
    }

    const entity = await this.ticketTierSvc.create(priceInput);

    response.json(entity);
  }

  @Get()
  async get(
    @Query('offset', ParseIntPipe) offset: number,
    @Query('limit', ParseIntPipe) limit: number,
    @Res() response: Response,
  ) {
    const batch = this.ticketTierSvc.filter(offset, limit);
    response.json(batch);
  }

  @Delete('/:uid')
  @HttpCode(204)
  async drop(@Param() params) {
    await this.ticketTierSvc.dropByUID(params.uid);
  }

  @Get('/:uid')
  async getOne(@Param() params, @Res() response: Response) {
    const record = await this.ticketTierSvc.getByUID(params.uid);

    if (record === null) {
      response.status(HttpStatus.NOT_FOUND).send();
      return;
    }

    response.json(record);
  }

  @Put('/:uid')
  @HttpCode(200)
  async update(
    @Param() params,
    @Body() body: PriceDTO,
    @Res() response: Response,
  ): Promise<TicketTierEntity> {
    const validity = creationValidator.validate(body);
    if (validity.error) {
      response.status(HttpStatus.BAD_REQUEST).json(validity.error);
      return;
    }

    const priceInput = new PriceInput();

    if (body.buyerPrice !== undefined) {
      priceInput.buyerPrice = Currency(body.buyerPrice);
    } else {
      priceInput.promoterRevenue = Currency(body.promoterRevenue);
    }

    const entity = await this.ticketTierSvc.getByUID(params.uid);

    if (entity === null) {
      response.status(HttpStatus.NOT_FOUND).send();
      return;
    }

    await this.ticketTierSvc.update(entity, priceInput);

    response.json(entity);
  }
}
