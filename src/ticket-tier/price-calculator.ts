import { Price } from '../database/entities/ticketTier.entity';
import { Settings } from '../shared/settings/settings.service';
import * as Currency from 'currency.js';

export function calculateFromBuyerPrice(
  buyerPrice: Currency,
  settings: Settings,
): Price {
  if (buyerPrice.subtract(settings.minimumFee).intValue <= 0) {
    throw new Error('Buyer price is too low to cover minimal fee');
  }

  let serviceFee = buyerPrice.multiply(settings.serviceFeeRate);
  if (serviceFee.subtract(settings.minimumFee).intValue < 0) {
    serviceFee = settings.minimumFee;
  }

  const price = new Price();
  price.serviceFee = serviceFee.value;
  price.buyerPrice = buyerPrice.value;
  price.promoterRevenue = buyerPrice.subtract(serviceFee).value;

  return price;
}

export function calculateFromPromoterRevenue(
  revenue: Currency,
  settings: Settings,
): Price {
  const feePercentage = 100 - settings.serviceFeeRate * 100;
  let buyerPrice = revenue.divide(feePercentage / 100);
  let serviceFee = buyerPrice.multiply(settings.serviceFeeRate);
  if (serviceFee.subtract(settings.minimumFee).intValue < 0) {
    serviceFee = settings.minimumFee;
    buyerPrice = revenue.add(serviceFee);
  }

  const price = new Price();
  price.serviceFee = serviceFee.value;
  price.buyerPrice = buyerPrice.value;
  price.promoterRevenue = revenue.value;

  return price;
}
