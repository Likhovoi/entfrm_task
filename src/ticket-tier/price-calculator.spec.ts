import {
  calculateFromBuyerPrice,
  calculateFromPromoterRevenue,
} from './price-calculator';
import { Settings } from '../shared/settings/settings.service';
import * as Currency from 'currency.js';

describe('calculate by buyer price', () => {
  it('should calculate when there are no digits after decimal place', function () {
    const settings = new Settings();
    settings.serviceFeeRate = 0.1;
    settings.minimumFee = Currency(2.5);

    const price = calculateFromBuyerPrice(Currency(50), settings);

    expect(price.buyerPrice).toEqual(50);
    expect(price.serviceFee).toEqual(5);
    expect(price.promoterRevenue).toEqual(45);
  });

  it('should calculate when there are digits after decimal place', function () {
    const settings = new Settings();
    settings.serviceFeeRate = 0.1;
    settings.minimumFee = Currency(2.5);

    const price = calculateFromBuyerPrice(Currency(50.35), settings);

    expect(price.buyerPrice).toEqual(50.35);
    expect(price.serviceFee).toEqual(5.04);
    expect(price.promoterRevenue).toEqual(45.31);
  });

  it('should throw when buyer price is less or equal to minimal fee', function () {
    const settings = new Settings();
    settings.serviceFeeRate = 0.1;
    settings.minimumFee = Currency(2.5);

    expect(() => calculateFromBuyerPrice(Currency(2.5), settings)).toThrow();
  });

  it('should set service fee to minimal one', function () {
    const settings = new Settings();
    settings.serviceFeeRate = 0.2;
    settings.minimumFee = Currency(3.2);

    const price = calculateFromBuyerPrice(Currency(5.4), settings);

    expect(price.buyerPrice).toEqual(5.4);
    expect(price.serviceFee).toEqual(3.2);
    expect(price.promoterRevenue).toEqual(2.2);
  });
});

describe('calculate by promoter revenue', () => {
  it('should calculate when there are no digits after decimal place', function () {
    const settings = new Settings();
    settings.serviceFeeRate = 0.1;
    settings.minimumFee = Currency(2.5);

    const price = calculateFromPromoterRevenue(Currency(90), settings);

    expect(price.buyerPrice).toEqual(100);
    expect(price.serviceFee).toEqual(10);
    expect(price.promoterRevenue).toEqual(90);
  });

  it('should calculate when there are digits after decimal place', function () {
    const settings = new Settings();
    settings.serviceFeeRate = 0.1;
    settings.minimumFee = Currency(2.5);

    const price = calculateFromPromoterRevenue(Currency(50.35), settings);

    expect(price.buyerPrice).toEqual(55.94);
    expect(price.serviceFee).toEqual(5.59);
    expect(price.promoterRevenue).toEqual(50.35);
  });

  it('should calculate when service fee is less or equal to minimal fee', function () {
    const settings = new Settings();
    settings.serviceFeeRate = 0.1;
    settings.minimumFee = Currency(2.5);

    const price = calculateFromPromoterRevenue(Currency(5.1), settings);

    expect(price.buyerPrice).toEqual(7.6);
    expect(price.serviceFee).toEqual(2.5);
    expect(price.promoterRevenue).toEqual(5.1);
  });
});
