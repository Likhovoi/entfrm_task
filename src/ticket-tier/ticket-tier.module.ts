import { Module } from '@nestjs/common';
import { TicketTierService } from './ticket-tier.service';
import { TicketTierController } from './ticket-tier.controller';
import { SharedModule } from '../shared/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingEntity } from '../database/entities/setting.entity';
import { TicketTierEntity } from '../database/entities/ticketTier.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([TicketTierEntity, SettingEntity]),
    SharedModule,
  ],
  providers: [TicketTierService],
  controllers: [TicketTierController],
})
export class TicketTierModule {}
