import { Inject, Injectable } from '@nestjs/common';
import { SettingsHolder } from '../shared/settings/settings.service';
import {
  Price,
  TicketTierEntity,
} from '../database/entities/ticketTier.entity';
import {
  calculateFromBuyerPrice,
  calculateFromPromoterRevenue,
} from './price-calculator';
import Currency from 'currency.js';
import { SETTINGS_TARGET } from '../shared/shared.module';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

export class PriceInput {
  buyerPrice?: Currency;
  promoterRevenue?: Currency;
}

export interface Batch {
  total: number;
  records: TicketTierEntity[];
}

@Injectable()
export class TicketTierService {
  constructor(
    @InjectRepository(TicketTierEntity)
    private readonly ticketTierRepository: Repository<TicketTierEntity>,
    @Inject(SETTINGS_TARGET)
    private readonly settingsHolder: SettingsHolder,
  ) {}

  async create(input: PriceInput): Promise<TicketTierEntity> {
    const settings = this.settingsHolder.get();

    let price: Price;

    if (input.buyerPrice !== undefined) {
      price = calculateFromBuyerPrice(input.buyerPrice, settings);
    } else if (input.promoterRevenue !== undefined) {
      price = calculateFromPromoterRevenue(input.promoterRevenue, settings);
    }

    const entity = new TicketTierEntity();
    entity.price = price;

    await this.ticketTierRepository.save(entity);

    return entity;
  }

  async update(entity: TicketTierEntity, input: PriceInput) {
    const settings = this.settingsHolder.get();

    let price: Price;

    if (input.buyerPrice !== undefined) {
      price = calculateFromBuyerPrice(input.buyerPrice, settings);
    } else if (input.promoterRevenue !== undefined) {
      price = calculateFromPromoterRevenue(input.promoterRevenue, settings);
    }

    entity.price = price;

    await this.ticketTierRepository.save(entity);
  }

  async getByUID(uuid: string): Promise<TicketTierEntity> {
    const qb = this.ticketTierRepository.createQueryBuilder();
    return qb.where('uuid = :uid', { uid: uuid }).getOne();
  }

  async dropByUID(uuid: string) {
    const qb = this.ticketTierRepository.createQueryBuilder();
    await qb.where('uuid = :uid', { uid: uuid }).delete().execute();
  }

  async filter(offset: number, limit: number): Promise<Batch> {
    const qb = this.ticketTierRepository.createQueryBuilder();

    const total = await qb.getCount();
    const records = await qb.offset(offset).limit(limit).getMany();

    return {
      total,
      records,
    };
  }
}
