import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1680263427375 implements MigrationInterface {
  name = 'init1680263427375';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "ticket_tiers" (
    "uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), 
    "buyer_price" numeric(10,2) NOT NULL, 
    "service_fee" numeric(10,2) NOT NULL, 
    "promoter_revenue" numeric(10,2) NOT NULL, 
    CONSTRAINT "PK_696c70fb15e9d2f95850e8540c0" PRIMARY KEY ("uuid")
)`,
    );

    await queryRunner.query(
      `CREATE TABLE "settings" (
    "key" character varying NOT NULL, 
    "value" character varying NOT NULL, 
    CONSTRAINT "PK_c8639b7626fa94ba8265628f214" PRIMARY KEY ("key")
)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "settings"`);
    await queryRunner.query(`DROP TABLE "ticket_tiers"`);
  }
}
