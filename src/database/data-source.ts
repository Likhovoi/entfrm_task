import { TicketTierEntity } from './entities/ticketTier.entity';
import { SettingEntity } from './entities/setting.entity';
import { init1680263427375 } from './migrations/1680263427375-init';
import { DataSource, DataSourceOptions } from 'typeorm';

export const AppdataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  entities: [TicketTierEntity, SettingEntity],
  migrations: [init1680263427375],
  synchronize: false,
  logging: false,
  subscribers: [],
};

export const AppDataSource = new DataSource(AppdataSourceOptions);
