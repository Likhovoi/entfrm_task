import { Entity, Column, PrimaryColumn, Generated } from 'typeorm';

export class Price {
  @Column({ name: 'buyer_price', type: 'decimal', precision: 10, scale: 2 })
  buyerPrice: number;

  @Column({ name: 'service_fee', type: 'decimal', precision: 10, scale: 2 })
  serviceFee: number;

  @Column({
    name: 'promoter_revenue',
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  promoterRevenue: number;
}

@Entity({
  name: 'ticket_tiers',
})
export class TicketTierEntity {
  @PrimaryColumn({ type: 'uuid' })
  @Generated('uuid')
  uuid: string;

  @Column(() => Price, {
    prefix: '',
  })
  price: Price;
}
