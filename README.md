
## Commands:

> **Run locally**
> 
> env $(cat .env.example) ts-node src/main.ts

> **Send "Create ticket tier" request**
> 
> curl -X POST http://localhost:3000/ticket-tier  -H "Content-Type: application/json" -d '{"buyerPrice": 20.1}'

> **Send "Get ticket tiers" request**
> 
> curl 'http://localhost:3000/ticket-tier?offset=0&limit=10'

> **Send "Get ticket tier by UID" request**
> 
>  curl 'http://localhost:3000/ticket-tier/e0eb46e0-c1e2-42ee-9661-09049ba4c80f'

> **Send "Delete ticket tier by UID" request**
> 
>  curl -X DELETE 'http://localhost:3000/ticket-tier/e0eb46e0-c1e2-42ee-9661-09049ba4c80f'

> **Send "Update ticket tier by UID" request**
>
> curl -X PUT http://localhost:3000/ticket-tier/cba97445-b7a7-4778-94f1-030693622e6a  -H "Content-Type: application/json" -d '{"promoterRevenue": 34.4}'

> **Migrate locally:**
> 
> env $(cat .env.example) typeorm-ts-node-commonjs migration:run -d ./src/database/data-source.ts

> **Generate new migration:**
>
> typeorm-ts-node-commonjs migration:generate ./src/database/migrations/$PUT_YOUR_NAME -d ./src/database/data-source.ts